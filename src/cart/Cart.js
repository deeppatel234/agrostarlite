import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus, faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { addToCart, removeToCart } from '../redux/actions';

import {
  CartContainer,
  CartTitle,
  Products,
  ProductImage,
  Product,
  Name,
  Price,
  QtyButton,
  UpdateQty,
  CartTotal,
  ProductDetails,
} from './CartStyled';

import { EmptyMessage } from '../base/BaseStyled';

class Cart extends Component {
  constructor(props) {
    super(props);

    this.onAddBtnClick = this.onAddBtnClick.bind(this);
    this.onRemoveBtnClick = this.onRemoveBtnClick.bind(this);
  }

  onAddBtnClick(ev) {
    const { dataset } = ev.currentTarget;
    const { addToCart } = this.props;
    addToCart(dataset.productid);
  }

  onRemoveBtnClick(ev) {
    const { dataset } = ev.currentTarget;
    const { removeToCart } = this.props;
    removeToCart(dataset.productid);
  }

  render() {
    const { shoppingCart } = this.props;

    const totalItems = shoppingCart.reduce((acc, product) => acc + product.qty, 0);
    const totalPrice = shoppingCart.reduce((acc, product) => acc + (product.qty * product.sellingPrice), 0);

    if (!totalItems) {
      return (
        <EmptyMessage>
          <p>
            <FontAwesomeIcon icon={faShoppingCart} />
          </p>
          Your Cart is empty
        </EmptyMessage>
      )
    }

    return (
      <CartContainer>
        <CartTitle>Your Cart</CartTitle>
        <Products>
          {
            shoppingCart.map((product) => {
              return (
                <Product key={product.skuCode}>
                  <ProductImage alt="product" src={product.productImages[0].name} />
                  <ProductDetails>
                    <Name>
                      {product.productName}
                    </Name>
                    <Price>
                      Rs.{product.sellingPrice}
                    </Price>
                    <QtyButton>
                      <UpdateQty data-productid={product.skuCode} onClick={this.onRemoveBtnClick}>
                        <FontAwesomeIcon icon={faMinus} />
                      </UpdateQty>
                      {product.qty}
                      <UpdateQty data-productid={product.skuCode} onClick={this.onAddBtnClick}>
                        <FontAwesomeIcon icon={faPlus} />
                      </UpdateQty>
                    </QtyButton>
                  </ProductDetails>
                </Product>
              )
            })
          }
        </Products>
        <CartTotal>
          <span>
            Total Qty: {totalItems}
          </span>
          <span>
            Total Amount: RS.{totalPrice}
          </span>
        </CartTotal>
      </CartContainer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shoppingCart: state,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: product => dispatch(addToCart(product)),
    removeToCart: productId => dispatch(removeToCart(productId)),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);
