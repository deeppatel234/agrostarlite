import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';

import { CartDetails, CartLink } from './CartStyled';

function CartPanel(props) {
  const totalItems = props.shoppingCart.reduce((acc, product) => acc + product.qty, 0);
  const totalPrice = props.shoppingCart.reduce((acc, product) => acc + (product.qty * product.sellingPrice), 0);

  if (!totalItems) {
    return false;
  }

  return (
    <CartDetails >
      <CartLink to="/cart">
        <span>{totalItems} Items | Rs. {totalPrice}</span>
        <span>VIEW BAG <FontAwesomeIcon icon={faAngleRight} /></span>
      </CartLink>
    </CartDetails>
  );
}

const mapStateToProps = (state) => {
  return {
    shoppingCart: state,
  }
}

export default connect(mapStateToProps)(CartPanel);
