import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ThemeVariables from '../base/ThemeVariables';
import { LightenDarkenColor } from '../base/Utils';

export const CartDetails = styled.div`
  position: fixed;
  width: 100%;
  bottom: 90px;
  display: flex;
  justify-content: center;
`

export const CartLink = styled(Link)`
  background-color: ${ThemeVariables.themeColor};
  width: 90%;
  padding: 13px 10px;
  color: #fff;
  font-size: 15px;
  border-radius: 7px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-decoration: none;
`

export const CartContainer = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 0 20px;
`

export const CartTitle = styled.p`
  font-size: 25px;
  border-bottom: 1px solid gray;
  padding-bottom: 10px;
`

export const Products = styled.div`
  padding: 10px 0;
`

export const Product = styled.div`
  display: flex;
  background-color: #fff;
  padding: 5px;
  margin: 5px;
`

export const ProductImage = styled.img`
  height: 80px;
`

export const Price = styled.p`
  font-weight: bold;
  font-size: 17px;
  margin: 4px 0;
`

export const Name = styled.p`
  margin: 0;
  margin-top: 4px;
  font-size: 17px;
`

export const QtyButton = styled.div`
  border: 2px solid ${ThemeVariables.themeColor};
  background-color: #fff;
  width: 50%;
  font-size: 13px;
  color: ${ThemeVariables.themeColor};
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const UpdateQty = styled.button`
  padding: 7px;
  background-color: ${LightenDarkenColor(ThemeVariables.themeColor, 90)};
  font-size: 13px;
  font-weight: bold;
  border: none;
`

export const CartTotal = styled.p`
  display: flex;
  justify-content: space-between;
  font-size: 18px;
`

export const ProductDetails = styled.div`
  flex: 1;
  padding: 0 10px;
`
