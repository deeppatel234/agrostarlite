import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import reducers from './reducers'

const reduxDevTool = composeWithDevTools(applyMiddleware(thunk));
const store = createStore(reducers, reduxDevTool);

export default store;