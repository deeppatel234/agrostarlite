import { ADD_TO_CART, REMOVE_FROM_CART } from './constants';
import Request from '../base/Request';

export default function shoppingCart(state = Request.getCacheShoppingCart(), action) {
  switch (action.type) {
    case ADD_TO_CART:
      let newAddState;
      if (typeof action.product === 'string') {
        newAddState = newAddState = state.map((product) => {
          if (product.skuCode === action.product) {
            product.qty += 1;
          }
          return product;
        });;
      } else if (state.findIndex(product => (product.skuCode === action.product.skuCode) && product.qty) !== -1) {
        newAddState = state.map((product) => {
          if (product.skuCode === action.product.skuCode) {
            product.qty += 1;
          }
          return product;
        });
      } else {
        newAddState = [...state, { ...action.product, qty: 1 }];
      }
      Request.storeShoppingCart(newAddState);
      return newAddState
    case REMOVE_FROM_CART:
      let newState;
      if (state.findIndex(product => (product.skuCode === action.productId) && product.qty === 1) !== -1) {
        newState = state.filter(product => product.skuCode !== action.productId);
      } else {
        newState = state.map((product) => {
          if (product.skuCode === action.productId) {
            product.qty -= 1;
          }
          return product;
        });

      }
      Request.storeShoppingCart(newState);
      return newState;
    default:
      return state
  }
}