import { ADD_TO_CART, REMOVE_FROM_CART } from './constants';

export const addToCart = function (product) {
  return {
    product,
    type: ADD_TO_CART,
  };
};

export const removeToCart = function (productId) {
  return {
    productId,
    type: REMOVE_FROM_CART,
  };
};