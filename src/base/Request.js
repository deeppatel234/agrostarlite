class Request {
  fetchProducts() {
    return new Promise((resolve, reject) => {
      if (this.cacheProduct) {
        resolve(this.cacheProduct);
      }

      fetch('https://www.mocky.io/v2/5b3de5ed310000db1f6de257')
        .then(res => res.json())
        .then((data) => {
          this.cacheProduct = data.responseData.productList;
          resolve(this.cacheProduct);
        }).catch(err => reject(err));
    });
  }
  // *** Note ***
  // Make API request for product search
  // But in this case search product from product cache
  searchProduct(searchText) {
    return new Promise((resolve) => {
      if (searchText.trim().length)
        resolve(this.cacheProduct.filter(product => product.productName.toLowerCase().includes(searchText.toLowerCase())));
      else
        resolve(this.cacheProduct);
    });
  }

  getCacheShoppingCart() {
    const cart = localStorage.getItem('cart');
    if (cart) {
      return JSON.parse(cart);
    }
    return [];
  }

  storeShoppingCart(cart) {
    return localStorage.setItem('cart', JSON.stringify(cart));
  }
}

export default new Request();
