import styled from 'styled-components';

export const EmptyMessage = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 20px;
  color: #a9a9a9;
  height: 100%;
  width: 100%;

  p {
    margin: 5px;
    font-size: 30px;
  }
`