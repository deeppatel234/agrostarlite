import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import ProductCard from './ProductCard';
import CartPanel from '../cart/CartPanel';

import Request from '../base/Request';
import { debounce } from '../base/Utils';

import {
  SearchBar,
  SearchInput,
  SearchIcon,
  ProductContainer,
  ProductCards,
} from './ProductStyled';

import { EmptyMessage } from '../base/BaseStyled';

class ProductPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      products: false,
      searchText: '',
    };

    this.onSearchInput = this.onSearchInput.bind(this);
    this.searchProduct = debounce(this.searchProduct.bind(this), 500);
  }

  componentDidMount() {
    Request.fetchProducts()
      .then(products => this.setState({ products }))
      .catch(() => this.setState({ error: 'Error in fetching products' }));
  }

  onSearchInput(ev) {
    const { value } = ev.target;
    this.setState({ searchText: value });
    this.searchProduct();
  }

  searchProduct() {
    const { searchText } = this.state;
    Request.searchProduct(searchText).then(products => this.setState({ products }));
  }

  render() {
    const { products, searchText, error } = this.state;

    if (error) {
      return <EmptyMessage>{error}</EmptyMessage>;
    }

    if (!products) {
      return <EmptyMessage>Loading...</EmptyMessage>;
    }

    return (
      <React.Fragment>
        <SearchBar>
          <SearchInput placeholder="Search by product name" value={searchText} onChange={this.onSearchInput} />
          <SearchIcon>
            <FontAwesomeIcon icon={faSearch} />
          </SearchIcon>
        </SearchBar>
        <ProductContainer>
          {
            products.length === 0 && <EmptyMessage>No Product Found</EmptyMessage>
          }
          <ProductCards>
            {
              products.length !== 0 && products.map((product) => {
                return (
                  <ProductCard key={product.skuCode} product={product} />
                )
              })
            }
          </ProductCards>
        </ProductContainer>
        <CartPanel />
      </React.Fragment>
    );
  }
}

export default ProductPage;
