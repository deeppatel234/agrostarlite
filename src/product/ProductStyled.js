import styled from 'styled-components'

import ThemeVariables from '../base/ThemeVariables';
import { LightenDarkenColor } from '../base/Utils';


export const Card = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin: 5px;
  width: calc(50% - 10px);
  padding: 10px;
  background-color: #fff;
  overflow: hidden;

  /* Medium devices (landscape tablets, 768px and up) */
  @media only screen and (min-width: 768px) {
    width: calc(25% - 10px);
  }

  /* Large devices (laptops/desktops, 992px and up) */
  @media only screen and (min-width: 1200px) {
    width: calc(16.66% - 10px);
  }


  img {
    height: 180px;
    width: 100%;
  }
`

export const ProductDetails = styled.div`
  flex: 1;
`

export const Price = styled.p`
  font-weight: bold;
  font-size: 15px;
  margin: 4px 0;
`

export const Name = styled.p`
  margin: 0;
  margin-top: 4px;
  font-size: 13px;
`

export const OfferLable = styled.span`
  position: absolute;
  left: -12px;
  top: 4px;
  background-color: #f5a641;
  padding: 5px 11px 5px 20px;
  font-size: 14px;
  border-radius: 5px;
`

export const AddButton = styled.button`
  width: 100%;
  border: 2px solid ${ThemeVariables.themeColor};
  background-color: #fff;
  padding: 7px;
  font-size: 13px;
  color: ${ThemeVariables.themeColor};
`

export const QtyButton = styled.div`
  border: 2px solid ${ThemeVariables.themeColor};
  background-color: #fff;
  font-size: 13px;
  color: ${ThemeVariables.themeColor};
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const ChangeQty = styled.button`
  padding: 7px;
  background-color: ${LightenDarkenColor(ThemeVariables.themeColor, 90)};
  font-size: 13px;
  font-weight: bold;
  border: none;
`

export const SearchBar = styled.div`
  position: relative;
  width: 100%;
  padding: 10px 20px;
`

export const SearchInput = styled.input`
  width: 100%;
  padding: 9px 30px 9px 9px;
  font-size: 13px;
`

export const SearchIcon = styled.span`
  position: absolute;
  right: 30px;
  font-size: 16px;
  top: 17px;
`

export const ProductContainer = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 0 20px;
`

export const ProductCards = styled.div`
  display: flex;
  flex-wrap: wrap;
`