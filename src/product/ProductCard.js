import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';

import { addToCart, removeToCart } from '../redux/actions';

import {
  Card,
  OfferLable,
  ProductDetails,
  Name,
  Price,
  QtyButton,
  ChangeQty,
  AddButton,
} from './ProductStyled';

class ProductCard extends Component {
  constructor(props) {
    super(props);

    this.onAddBtnClick = this.onAddBtnClick.bind(this);
    this.onRemoveBtnClick = this.onRemoveBtnClick.bind(this);
  }

  onAddBtnClick() {
    const { addToCart, product } = this.props;
    addToCart(product);
  }

  onRemoveBtnClick() {
    const { removeToCart, product } = this.props;
    removeToCart(product.skuCode);
  }

  render() {
    const { product, cartQty } = this.props;

    return (
      <Card>
        {
          product.defaultOffer && <OfferLable>{product.defaultOffer}</OfferLable>
        }
        <img alt="product" src={product.productImages[0].name} />
        <ProductDetails>
          <Name>
            {product.productName}
          </Name>
          <Price>
            Rs.{product.sellingPrice}
          </Price>
        </ProductDetails>
        {
          cartQty
            ? (
              <QtyButton>
                <ChangeQty onClick={this.onRemoveBtnClick}>
                  <FontAwesomeIcon icon={faMinus} />
                </ChangeQty>
                {cartQty}
                <ChangeQty onClick={this.onAddBtnClick}>
                  <FontAwesomeIcon icon={faPlus} />
                </ChangeQty>
              </QtyButton>
            )
            : <AddButton onClick={this.onAddBtnClick}>Add</AddButton>
        }
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const curentProduct = state.find(p => p.skuCode === ownProps.product.skuCode);
  return {
    cartQty: curentProduct && curentProduct.qty,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: product => dispatch(addToCart(product)),
    removeToCart: productId => dispatch(removeToCart(productId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);
