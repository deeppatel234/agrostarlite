import ThemeVariables from '../base/ThemeVariables';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const Footer = styled.footer`
  display: flex;
  box-shadow: 0 -2px 8px #dbdbdb;
`

export const FooterButton = styled(Link)`
  width: 50%;
  font-size: 0.7rem;
  padding: 10px;
  background-color: #fff;
  border: none;
  color: gray;
  font-weight: bold;
  text-align: center;
  text-decoration: none;

  ${props => props.activebtn && css`
    color: ${ThemeVariables.themeColor};
  `}

  p {
    margin: 0;
    margin-top: 5px;
  }

  svg {
    font-size: 20px;
  }
`

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #dcdcdc;
  padding: 10px;
  background-color: #fff;
  box-shadow: 0 2px 8px #dbdbdb;
`

export const HeaderButton = styled.a`
  font-size: 1rem;
  padding: 0 10px;
`

export const Logo = styled.img`
  height: 30px;
`