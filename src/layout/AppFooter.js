import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStoreAlt, faShoppingBag } from '@fortawesome/free-solid-svg-icons';

import { withRouter } from 'react-router-dom';

import { Footer, FooterButton } from './LayoutStyled';

function AppFooter(props) {
  const {
    location,
  } = props;

  const currentPage = location.pathname.replace('/', '') || 'home';

  return (
    <Footer>
      <FooterButton to='/' activebtn={currentPage === 'home' ? 'active' : ''}>
        <FontAwesomeIcon icon={faStoreAlt} />
        <p>Products</p>
      </FooterButton>
      <FooterButton to='/cart' activebtn={currentPage === 'cart' ? 'active' : ''}>
        <FontAwesomeIcon icon={faShoppingBag} />
        <p>My Orders</p>
      </FooterButton>
    </Footer>
  );
}

export default withRouter(AppFooter);
