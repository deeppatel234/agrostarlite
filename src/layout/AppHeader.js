import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedoAlt, faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

import { Header, Logo, HeaderButton } from './LayoutStyled';


class AppHeader extends Component {
  render() {
    return (
      <Header>
        <Link to="/"><Logo alt="logo" src="/logo.png" /></Link>
        <div>
          <HeaderButton>
            <FontAwesomeIcon icon={faRedoAlt} />
          </HeaderButton>
          <HeaderButton>
            <FontAwesomeIcon icon={faUserAlt} />
          </HeaderButton>
        </div>
      </Header>
    );
  }
}

export default AppHeader;
