import React from 'react';

import { mount } from 'enzyme';
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { enzymeFind } from 'styled-components/test-utils'
import { MemoryRouter } from 'react-router';

import appStore from './redux/store';

import ProductCard from './product/ProductCard';
import {
  Name,
  Price,
  AddButton,
  QtyButton,
  ChangeQty,
} from './product/ProductStyled';

import CartPanel from './cart/CartPanel';
import { CartLink } from './cart/CartStyled';

const product = {
  "levelOneCategory": "Category",
  "defaultOffer": 'Buy 1 get 1 free',
  "skuCode": "AGS-CN-004",
  "productImages": [
    {
      "type": "thumb",
      "name": "#"
    }
  ],
  "sellingPrice": 450,
  "qty": 3,
  "productName": "Power Gel - Plant Nutrient (500 Ml)"
};

describe('<ProductCard />',() => {
  let wrapper

  beforeEach(async ()=>{
    wrapper = mount(<Provider store={appStore}><ProductCard product={product} /></Provider>)
  });

  it('should have all basic product details', () => {
    expect(enzymeFind(wrapper, Name).text()).toBe('Power Gel - Plant Nutrient (500 Ml)');
    expect(enzymeFind(wrapper, Price).text()).toBe('Rs.450');
    expect(enzymeFind(wrapper, AddButton).exists()).toBeTruthy();
    expect(enzymeFind(wrapper, QtyButton).exists()).toBeFalsy();
  });

  it('should have quantity buttons when add product', () => {
    enzymeFind(wrapper, AddButton).simulate('click');
    expect(enzymeFind(wrapper, QtyButton).exists()).toBeTruthy();
    expect(enzymeFind(wrapper, QtyButton).text()).toBe('1');
    expect(enzymeFind(wrapper, AddButton).exists()).toBeFalsy();
    expect(enzymeFind(wrapper, ChangeQty).exists()).toBeTruthy();
  });

  it('increment and decrement product quantity', () => {
    enzymeFind(wrapper, ChangeQty).at(1).simulate('click');
    expect(enzymeFind(wrapper, QtyButton).text()).toBe('2');
    enzymeFind(wrapper, ChangeQty).at(0).simulate('click');
    expect(enzymeFind(wrapper, QtyButton).text()).toBe('1');
  });

  it('remove quantity buttons when product quantity is zero', () => {
    enzymeFind(wrapper, ChangeQty).at(0).simulate('click');
    expect(enzymeFind(wrapper, AddButton).exists()).toBeTruthy();
    expect(enzymeFind(wrapper, QtyButton).exists()).toBeFalsy();
  });
});


describe('<CartPanel /> Redux Test',() => {
  let wrapper
  const initialState = [product]
  const mockStore = configureStore()
  let store

  beforeEach(async ()=>{
    store = mockStore(initialState)
    wrapper = mount(<Provider store={store}><MemoryRouter initialEntries={[ '/' ]}><CartPanel /></MemoryRouter></Provider>)
  })

  it('should have quantity and price details', () => {
    expect(enzymeFind(wrapper, CartLink).at(0).text()).toEqual(expect.stringContaining('3 Items | Rs. 1350'));
  });
});
