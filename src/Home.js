import React, { Component } from 'react';
import { createGlobalStyle } from 'styled-components'

import { Provider } from 'react-redux';
import ProductPage from './product/ProductPage';
import AppFooter from './layout/AppFooter';
import AppHeader from './layout/AppHeader';
import Cart from './cart/Cart';
import store from './redux/store';
import { BrowserRouter, Route } from 'react-router-dom'


const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
  }
  body {
    margin: 0;
    background-color: #ededed;
    height: 100%;
  }
  #root {
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`

class Home extends Component {
  render() {
    return (
      <Provider store={store}>
        <GlobalStyle />
        <BrowserRouter>
          <React.Fragment>
            <AppHeader />
            <Route exact path="/" component={ProductPage} />
            <Route path="/cart" component={Cart} />
            <AppFooter />
          </React.Fragment>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default Home;
